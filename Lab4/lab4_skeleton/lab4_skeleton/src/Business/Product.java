/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

/**
 *
 * @author sanam
 */
public class Product {
    private static int count = 0;
    private String productName;
    private int price;
    private int modelNumber;
    
    public Product(){
        count++;
        modelNumber = count;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getModelNumber() {
        return modelNumber;
    }
    
    @Override
    public String toString(){
        return this.productName;
    }    
}
