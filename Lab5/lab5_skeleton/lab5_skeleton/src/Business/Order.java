/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author sanam
 */
public class Order {

    private ArrayList<OrderItem> orderItemList;
    private int orderNumber;
    private static int count = 0;

    public Order(){
        orderNumber = ++count;
        orderItemList = new ArrayList<OrderItem>();
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void removeOrderItem(OrderItem o) {
        orderItemList.remove(o);
    }

    public OrderItem addOrderItem(Product product, int quantity, int price) {
        OrderItem item = new OrderItem();
        item.setProduct(product);
        item.setQuantity(quantity);
        item.setSalesPrice(price);
        orderItemList.add(item);
        return item;
    }

}
